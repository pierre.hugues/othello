"use strict";

require("./lib/prototype");

const path = require("path");
const { app, ipcMain } = require("electron");

const Window = require("./lib/Window");
const MinMax = require("./lib/MinMax");


// Primary function
function main() {
    let win = new Window({
        file: path.join("public/views", "index.html"), 
        openDevTools: false, 
        width: 1600,
        height: 800, 
        webPreferences: { nodeIntegration: true}, 
        icon: path.join(__dirname, "/build/icons/icon.png")
    });

    ipcMain.on('AIturn', async (event, arg) => {
        let minmax = new MinMax(4, arg, win);
        let res = await minmax.start();
        event.reply('AIMove', res);
    });
}

// When Electron is ready, launch the function
app.on("ready", main);

// Close the application once all the windows are closed
app.on("window-all-closed", () => {
    app.quit();
});