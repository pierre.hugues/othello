'use strict';

const weights = require('./weights');

class Othello {

    // #region INIT

    constructor(container, params) {
        this.$container = container;
        this.params = params || {};
        this.visual = params.visual;
        this.boardConfig = params.boardConfig || 'default.js';

        // #region JQuery Variables

        this.$CellTemplate = $('<td class=\'cell\'></td>');
        this.$RowTemplate = $('<tr></tr>');
        this.$TokenTemplate = $('<div class=\'token\'></div>');

        // #endregion

        this.currentLayout = {
            players: {
                'black': {
                    idPlayer: 'Joueur',
                    score: 0,
                    color: 'black',
                    dom: $('#scores > #black > span'),
                },
                'white': {
                    idPlayer: 'IA',
                    score: 0,
                    color: 'white',
                    dom: $('#scores > #white > span'),
                },
            },
            board: {
                idBoard: '01',
                nbMove: 0,
                tokens: {},
                cells: {},
                turn: 'black',
                blocked: [],
            },
            cell: {
                width: this.$container.width() / 8,
                height: this.$container.height() / 8,
            },
        };

        this.$container.empty();

        // Build the table with all the cells
        for (let i = 0; i < 8; i++) {
            const row = this.$RowTemplate.clone()
                .width(`${this.$container.width()}px`);
            for (let j = 0; j < 8; j++) {
                const cell = this.$CellTemplate.clone()
                    .width(`${this.currentLayout.cell.width}px`)
                    .height(`${this.currentLayout.cell.height}px`)
                    .appendTo(row)
                    .attr('id', `cell_${i}${j}`);

                this.currentLayout.board.cells[`${i}${j}`] = {
                    id: `cell_${i}${j}`,
                    dom: cell,
                    row: i,
                    cell: j,
                    content: null,
                    weight: 0,
                    playable: false,
                };
            }
            row.appendTo(this.$container);
        }
        this.initTokens(this.boardConfig);
        this.initScores();
        this.initWeighting();
        this.playTurn();
    }

    /**
     * Initialize the first tokens
     * @author Pierre
     * @param {String} boardConfig Config of the initial tokens
     */
    initTokens(boardConfig) {
        // Init the first tokens
        let config;
        try {
            config = require('../public/boards/' + boardConfig);
        } catch (e) {
            console.warn('Could not find config : ' + boardConfig);
            config = require('../public/boards/default');
        }

        for (const token of Object.values(config)) {
            this.addToken(token.color, token.i, token.j);
        }
    }

    /**
     * Initialize the scoreboard
     * @author Pierre
     */
    initScores() {
        this.setScores();
    }

    /**
     * Initialize the weighting of the cells
     * @author Pierre
     */
    initWeighting() {
        for (const cell of Object.values(this.currentLayout.board.cells)) {
            cell.weight = weights[cell.id];
        }
    }

    /**
     * Repaint the board with the current config
     */
    repaintBoard(tokenList) {
        for(let token of tokenList) {
            this.addToken(token.color, token.row, token.cell);
        }
    }

    // #endregion

    // #region SETTERS

    /**
     * Set a cell playable or not
     * @param {Boolean} playable Playable cell
     * @param {Number} i Row number of the cell
     * @param {Number} j Column number of the cell
     */
    setCellPlayable(playable, i, j) {
        const cell = this.currentLayout.board.cells[`${i}${j}`];
        cell.playable = playable;
        if (playable) cell.dom.addClass('playable');
        else cell.dom.removeClass('playable');
    }

    /**
     * Update the scoreboard after a token placement
     * @author Pierre
     */
    setScores() {
        let cptWhite = 0;
        let cptBlack = 0;

        for (const token of Object.values(this.currentLayout.board.tokens)) {
            if (token.color === 'white') cptWhite++;
            else cptBlack++;
        }

        if(this.visual) {
            // Update score in data
            this.currentLayout.players['black'].score = cptBlack;
            this.currentLayout.players['white'].score = cptWhite;
    
            // Update dom
            this.currentLayout.players['black'].dom.html(cptBlack);
            this.currentLayout.players['white'].dom.html(cptWhite);
        }
    }

    /**
     * Reverse the captured tokens on the board
     * @author Pierre
     */
    reverseToken(i, j) {
        const token = this.currentLayout.board.tokens[`black_${i}_${j}`] || this.currentLayout.board.tokens[`white_${i}_${j}`];
        const color = token.color === 'black' ? 'white' : 'black';

        delete this.currentLayout.board.tokens[token.id];
        this.currentLayout.board.cells[`${i}${j}`].dom.html('');
        this.currentLayout.board.cells[`${i}${j}`].content = null;

        this.addToken(color, i, j);
        this.setScores();
    }

    /**
     * Display the winner of the game
     * @author Pierre 
     */
    setWinner(end) {
        if (end) {
            let winner = this.currentLayout.players.black.score >= this.currentLayout.players.white.score ? "black" : "white";
            $('#gameon').hide();
            $('#win').show();
            $('#win span').html(`${this.currentLayout.players[winner].idPlayer} (${winner})`);
        } else {
            $('#gameon').show();
            $('#win').hide();
            $('#gameon span').html(`${this.currentLayout.players[this.currentLayout.board.turn].idPlayer} (${this.currentLayout.board.turn})`);
        }
    }

    // #endregion

    // #region EVENT

    /**
     * Function called when a token is placed upon the board
     * @param {String} i Row number
     * @param {String} j Column number
     */
    clickOnCell(i, j) {
        if (i && j) {
            i = parseInt(i);
            j = parseInt(j);
            this.addToken(this.currentLayout.board.turn, i, j);
            this.reverseTokens(i, j, this.currentLayout.board.turn);
        }
        this.setScores();
        this.currentLayout.board.turn = this.currentLayout.board.turn === "black" ? "white" : "black";
        this.playTurn();
    }

    // #endregion

    // #region ADDERS

    /**
     * Put the token on the board (on the chosen cell)
     * @author Pierre
     * @param {String} color Color of the token
     * @param {Number} i Row number of the token
     * @param {Number} j Column number of the token
     */
    addToken(color, i, j) {
        const token = this.$TokenTemplate.clone()
            .css('background-color', color)
            .appendTo(this.currentLayout.board.cells[`${i}${j}`].dom);

        const options = {
            id: `${color}_${i}_${j}`,
            color: color,
            dom: token,
            parent: this.currentLayout.board.cells[`${i}${j}`].dom,
            row: i,
            cell: j,
        };

        this.currentLayout.board.tokens[options.id] = options;
        this.currentLayout.board.cells[`${i}${j}`].content = options;
        return options;
    }

    // #endregion

    // #region PLAY

    /**
     * Regroup all the methods which concern a player turn
     * @author Roxanne
     */
    playTurn() {
        for (let i = 0; i < 8; i++) {
            for (let j = 0; j < 8; j++) {
                this.setCellPlayable(false, i, j);
            }
        }
        if (this.currentLayout.board.blocked.length >= 2) {
            if(this.visual) this.setWinner(true);
        } else {
            if(this.visual)this.setWinner(false);
            this.displayPossibilities();
            if (this.currentLayout.board.blocked.indexOf(this.currentLayout.board.turn) !== -1) {
                this.clickOnCell();
            }
        }
    }

    /**
     * Returns all tokens captured by a specified token
     * @param {Number} i Row number
     * @param {Number} j Column number
     * @param {String} color Color of the token
     */
    reverseTokens(i, j, color) {
        let otherTokens = Object.values(this.currentLayout.board.tokens).filter((token) => token.color !== color);
        let toReverse = [];
        for (let token of otherTokens) {
            if (this.isSurrounded(token.row, token.cell, i, j, color)) {
                if(this.IsCapturable(token.row, token.cell, i, j, color)) {
                    toReverse.push(token);
                }
            }
        }

        for(let token of toReverse) {
            this.reverseToken(token.row, token.cell);
        }
    }

    /**
     * Check if the cel is capturable by the token placed
     * @param {*} iCell 
     * @param {*} jCell 
     * @param {*} iCell2 
     * @param {*} jCell2 
     * @param {*} colorCell 
     */
    IsCapturable(iCell, jCell, iCell2, jCell2, colorCell) {
        const diffI = iCell - iCell2;
        const diffJ = jCell - jCell2;

        if (Math.abs(diffI) >= 1 && Math.abs(diffJ) === 0) {
            // Check column
            if (diffI >= 1) {
                for (let i = iCell; i > iCell2; i--) {
                    if (!this.currentLayout.board.cells[`${i}${jCell}`].content) return false;
                    if (this.currentLayout.board.cells[`${i}${jCell}`].content && this.currentLayout.board.cells[`${i}${jCell}`].content.color == colorCell) {
                        return false;
                    }
                }
            } else {
                for (let i = iCell; i < iCell2; i++) {
                    if (!this.currentLayout.board.cells[`${i}${jCell}`].content) return false;
                    if (this.currentLayout.board.cells[`${i}${jCell}`].content && this.currentLayout.board.cells[`${i}${jCell}`].content.color == colorCell) {
                        return false;
                    }
                }
            }
        } else if (Math.abs(diffI) === 0 && Math.abs(diffJ) >= 1) {
            // Check line
            if (diffJ >= 1) {
                for (let j = jCell; j > jCell2; j--) {
                    if (!this.currentLayout.board.cells[`${iCell}${j}`].content) return false;
                    if (this.currentLayout.board.cells[`${iCell}${j}`].content && this.currentLayout.board.cells[`${iCell}${j}`].content.color == colorCell) {
                        return false;
                    }
                }
            } else {
                for (let j = jCell; j < jCell2; j++) {
                    if (!this.currentLayout.board.cells[`${iCell}${j}`].content) return false;
                    if (this.currentLayout.board.cells[`${iCell}${j}`].content && this.currentLayout.board.cells[`${iCell}${j}`].content.color == colorCell) {
                        return false;
                    }
                }
            }
        } else {
            let i;
            let j;
            if (diffI > -1 && diffJ > -1 && (diffI - diffJ === 0)) {
                // Check the diagonal up left
                i = iCell - 1;
                j = jCell - 1;

                while (i > iCell2 && j > jCell2) {
                    if (!this.currentLayout.board.cells[`${i}${j}`].content) return false;
                    if (this.currentLayout.board.cells[`${i}${j}`].content && this.currentLayout.board.cells[`${i}${j}`].content.color == colorCell) {
                        return false;
                    }

                    i = i - 1;
                    j = j - 1;
                }
            } else if (diffI > -1 && diffJ < 1 && (diffI + diffJ === 0)) {
                // Check the diagonal up right
                i = iCell - 1;
                j = jCell + 1;

                while (i > iCell2 && j < jCell2) {
                    if (!this.currentLayout.board.cells[`${i}${j}`].content) return false;
                    if (this.currentLayout.board.cells[`${i}${j}`].content && this.currentLayout.board.cells[`${i}${j}`].content.color == colorCell) {
                        return false;
                    }

                    i = i - 1;
                    j = j + 1;
                }
            } else if (diffI < 1 && diffJ > -1 && (diffI + diffJ === 0)) {
                // Check the diagonal down left
                i = iCell + 1;
                j = jCell - 1;

                while (i < iCell2 && j > jCell2) {
                    if (!this.currentLayout.board.cells[`${i}${j}`].content) return false;
                    if (this.currentLayout.board.cells[`${i}${j}`].content && this.currentLayout.board.cells[`${i}${j}`].content.color == colorCell) {
                        return false;
                    }

                    i = i + 1;
                    j = j - 1;
                }
            } else if (diffI < 1 && diffJ < 1 && (diffI - diffJ === 0)) {
                // Check the diagonal down right
                i = iCell + 1;
                j = jCell + 1;

                while (i < iCell2 && j < jCell2) {
                    if (!this.currentLayout.board.cells[`${i}${j}`].content) return false;
                    if (this.currentLayout.board.cells[`${i}${j}`].content && this.currentLayout.board.cells[`${i}${j}`].content.color == colorCell) {
                        return false;
                    }

                    i = i + 1;
                    j = j + 1;
                }
            }
        }

        return true;
    }

    /**
     * Display all the positions where the player can put a token
     * @author Pierre
     */
    displayPossibilities() {
        let possibilities = false;
        for (let i = 0; i < 8; i++) {
            for (let j = 0; j < 8; j++) {
                if (this.availablePlacements(i, j, this.currentLayout.board.turn)) {
                    possibilities = true;
                }
            }
        }
        if (!possibilities) {
            this.currentLayout.board.blocked.push(this.currentLayout.board.turn);
        } else {
            this.currentLayout.board.blocked = this.currentLayout.board.blocked.filter((color) => color !== this.currentLayout.board.turn);
        }
    }

    /**
     * Check if the placement is available for the player
     * @param {Number} iCell i of the cell
     * @param {Number} jCell j of the cell
     * @param {String} color color of the token
     * @author Roxanne
     */
    availablePlacements(iCell, jCell, color) {
        let check = false;

        if (this.currentLayout.board.cells[`${iCell}${jCell}`].content) return;
        const neighbour = this.getNeighbour(iCell, jCell, color);

        for (const neigh of neighbour) {
            if (this.isSurrounded(neigh.row, neigh.cell, iCell, jCell, color)) {
                this.setCellPlayable(true, iCell, jCell);
                check = true;
            }
        }

        return check;
    }

    /**
     * Get the neighbour of a cell
     * @author Roxanne
     * @author Pierre
     * @param {Number} iCell row number
     * @param {Number} jCell column number
     * @param {String} color color of the token
     */
    getNeighbour(iCell, jCell, color) {
        const neighbourTab = [];

        for (let i = iCell - 1; i <= iCell + 1; i++) {
            for (let j = jCell - 1; j <= jCell + 1; j++) {
                if (i == iCell && j == jCell) continue;
                const cell = this.currentLayout.board.cells[`${i}${j}`];
                if (cell && cell.content && cell.content.color !== color) {
                    neighbourTab.push(cell);
                }
            }
        }

        return neighbourTab;
    }

    /**
     * Return true if at least one token is reversable
     * @author Roxanne
     * @author Pierre
     * @param {Number} iCell
     * @param {Number} jCell
     * @param {String} colorCell
     */
    isSurrounded(iCell, jCell, iCell2, jCell2, colorCell) {
        const diffI = iCell - iCell2;
        const diffJ = jCell - jCell2;

        if (Math.abs(diffI) >= 1 && Math.abs(diffJ) === 0) {
            // Check column
            if (diffI >= 1) {
                for (let i = iCell; i < 8; i++) {
                    if (!this.currentLayout.board.cells[`${i}${jCell}`].content) break;
                    if (this.currentLayout.board.cells[`${i}${jCell}`].content && this.currentLayout.board.cells[`${i}${jCell}`].content.color == colorCell) {
                        return true;
                    }
                }
            } else {
                for (let i = iCell; i >= 0; i--) {
                    if (!this.currentLayout.board.cells[`${i}${jCell}`].content) break;
                    if (this.currentLayout.board.cells[`${i}${jCell}`].content && this.currentLayout.board.cells[`${i}${jCell}`].content.color == colorCell) {
                        return true;
                    }
                }
            }
        } else if (Math.abs(diffI) === 0 && Math.abs(diffJ) >= 1) {
            // Check line
            if (diffJ >= 1) {
                for (let j = jCell; j < 8; j++) {
                    if (!this.currentLayout.board.cells[`${iCell}${j}`].content) break;
                    if (this.currentLayout.board.cells[`${iCell}${j}`].content && this.currentLayout.board.cells[`${iCell}${j}`].content.color == colorCell) {
                        return true;
                    }
                }
            } else {
                for (let j = jCell; j >= 0; j--) {
                    if (!this.currentLayout.board.cells[`${iCell}${j}`].content) break;
                    if (this.currentLayout.board.cells[`${iCell}${j}`].content && this.currentLayout.board.cells[`${iCell}${j}`].content.color == colorCell) {
                        return true;
                    }
                }
            }
        } else {
            let i;
            let j;
            if (diffI <= -1 && diffJ <= -1 && (diffI - diffJ === 0)) {
                // Check the diagonal up left
                i = iCell - 1;
                j = jCell - 1;

                while (i >= 0 && j >= 0) {
                    if (!this.currentLayout.board.cells[`${i}${j}`].content) break;
                    if (this.currentLayout.board.cells[`${i}${j}`].content && this.currentLayout.board.cells[`${i}${j}`].content.color == colorCell) {
                        return true;
                    }

                    i = i - 1;
                    j = j - 1;
                }
            } else if (diffI <= -1 && diffJ >= 1 && (diffI + diffJ === 0)) {
                // Check the diagonal up right
                i = iCell - 1;
                j = jCell + 1;

                while (i >= 0 && j <= 7) {
                    if (!this.currentLayout.board.cells[`${i}${j}`].content) break;
                    if (this.currentLayout.board.cells[`${i}${j}`].content && this.currentLayout.board.cells[`${i}${j}`].content.color == colorCell) {
                        return true;
                    }

                    i = i - 1;
                    j = j + 1;
                }
            } else if (diffI >= 1 && diffJ <= -1 && (diffI + diffJ === 0)) {
                // Check the diagonal down left
                i = iCell + 1;
                j = jCell - 1;

                while (i <= 7 && j >= 0) {
                    if (!this.currentLayout.board.cells[`${i}${j}`].content) break;
                    if (this.currentLayout.board.cells[`${i}${j}`].content && this.currentLayout.board.cells[`${i}${j}`].content.color == colorCell) {
                        return true;
                    }

                    i = i + 1;
                    j = j - 1;
                }
            } else if (diffI >= 1 && diffJ >= 1 && (diffI - diffJ === 0)) {
                // Check the diagonal down right
                i = iCell + 1;
                j = jCell + 1;

                while (i <= 7 && j <= 7) {
                    if (!this.currentLayout.board.cells[`${i}${j}`].content) break;
                    if (this.currentLayout.board.cells[`${i}${j}`].content && this.currentLayout.board.cells[`${i}${j}`].content.color == colorCell) {
                        return true;
                    }

                    i = i + 1;
                    j = j + 1;
                }
            }
        }

        return false;
    }

    // #endregion

}

module.exports = Othello;