'use strict';

let Othello = require('./Othello');
const { ipcMain } = require("electron");

class MinMax {

    /**
     * Build a min max algorithm for the Othello
     * @param {Number} maxDepth Maximum depth for the min-max algorithm
     * @param {Othello} params State of the board
     */
    constructor(maxDepth, params, window) {
        this.game = params;
        this.depth = 0;
        this.maxDepth = maxDepth;
        this.window = window;
        this.memory = params;
    }

    /**
     * Start the min max algorithm with the initial possibilites for the AI
     */
    async start() {
        let bestMove = 0;
        let max = 0;
        let i = 0;

        let moves = Object.values(this.game.currentLayout.board.cells).filter((cell) => cell.playable); // Get the initial possibilities

        for(let move of moves) {
            let gain = await this.max(move);
            this.memory = this.game;
            if(gain > max) {
                max = gain;
                bestMove = i;
            }
            this.depth = 0;
            i++;
        }

        return moves[bestMove];
    }

    /**
     * Determine the move that gives the best value based on his successors
     * @param {*} n Move to maximize
     * @returns {Number} Maximal gain
     */
    async max(n) {
        try {
            if(this.end()) {
                let evaluation = await this.evaluate(n);
                return evaluation;
            }

            let v = -Infinity;
            let nodes = await this.extend(n);
            let memory2 = this.memory;
            this.depth++;
            for(let node of nodes) {
                this.memory = memory2;
                v = Math.max(v, await this.min(node));
            }
            return v;
        } catch (error) {

        }
    }

    /**
     * Determine the move that gives the worst value based on his successors
     * @param {*} n Move to minimize
     * @returns {Number} Minimal gain
     */
    async min(n) {
        try {
            if(this.end()) {
                let evaluation = await this.evaluate(n);
                return evaluation;
            }

            let v = Infinity;
            let nodes = await this.extend(n);
            let memory2 = this.memory;
            for(let node of nodes) {
                this.memory = memory2;
                v = Math.min(v, await this.max(node));
            }
            return v;
        } catch (error) {

        }
    }

    /**
     * Evaluate how much we gain by doing this move
     * @param {*} n Move to evaluate
     * @returns {Number} Gain of this move
     */
    evaluate(n) {
        return new Promise(async (resolve, reject) => {
            try {
                let empty = Object.values(this.memory.currentLayout.board.cells).filter((cell) => cell.content == null);

                if(empty.length >= 24) {
                    let newBoard = await this.simulate(n);
                    resolve((newBoard.currentLayout.players[this.game.currentLayout.board.turn].score - this.game.currentLayout.players[this.game.currentLayout.board.turn].score));
                } else {
                    resolve(this.game.currentLayout.board.cells[`${n.row}${n.cell}`].weight);
                }
            } catch (error) {
                let msg = `${error.message || error.error || error}`;
                reject(msg);
            }
        });
    }

    /**
     * Simulate a move and get the new list of possibilities
     * @param {*} n Move to simulate
     */
    async extend(n) {
        try {
            let newBoard = await this.simulate(n);
            let moves = Object.values(newBoard.currentLayout.board.cells).filter((cell) => cell.playable); // Get the new possibilities
            return moves;
        } catch(error) {

        }
    }

    /**
     * Create a new game based on the current one and a simulated move to find out the best one
     * @param {*} n Move to simulate
     */
    simulate(n) {
        return new Promise((resolve, reject) => {
            try {
                this.window.webContents.send('simulate', {cell: n, memory: this.memory});
                ipcMain.once('simulateResponse', (event, newGame) => {
                    this.memory = newGame;
                    resolve(newGame);
                });
            } catch (error) {
                let msg = `${error.message || error.error || error}`;
                reject(msg);
            }
        });
    }

    /**
     * Check if we arrived at the final depth
     * @returns {Boolean} True if the maximal depth is reached
     */
    end() {
        return this.depth == this.maxDepth;
    }

}

module.exports = MinMax;