# Othello

Small game of othello/reversi with use of different algorithms such as min-max and alpha beta to determine best move.

## Réalisation

Pierre HUGUES - Roxanne THOMAS - M1 TNSI FA

## Compilation

### Pré-requis

Node.js v12.x <https://nodejs.org/fr/download/>

### Compiler

- Se mettre au niveau du package.json
- npm run start