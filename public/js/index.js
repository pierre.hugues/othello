const { ipcRenderer  } = require('electron');

let Othello = require('../../lib/Othello');

/**
 * @type {Othello}
 */
let othelloGame;

function Init() {
    console.log("new board");
    othelloGame = new Othello($("#board"), {boardConfig: 'default', visual: true});
}

// Launch everything
Init();

// Reset the board if the button is pressed;
$("#reset").on('click', (e) => {
    $("#board").empty();
    console.log("board cleared");
    othelloGame = new Othello($("#board"), {visual: true});
    console.log("new board");
});

// Click on the board, check if it's the black turn and if he clicked on a cell he can play
$("#board").on('click', (e) => {
    if(e.target.className.indexOf("playable") !== -1 && othelloGame.currentLayout.board.turn !== "white") {
        othelloGame.clickOnCell(e.target.id.split('_')[1][0], e.target.id.split('_')[1][1]);
        ipcRenderer.send('AIturn', othelloGame); // Signal to the AI it's his turn
    }
});

// Receive the decision from the AI
ipcRenderer.on('AIMove', (event, args) => {
    othelloGame.clickOnCell(args.row, args.cell);
    $("#fakeboard").empty();
});

// Receive an event 'simulate', meaning he simulates the move for the AI to find the best course of action
ipcRenderer.on('simulate', (event, args) => {
    // Create a fake game based on the configuration sent
    let fakeOthello = new Othello($("#fakeboard"), {boardConfig: 'empty', visual: false});
    let fakeOthelloMemory = args.memory;
    fakeOthello.repaintBoard(Object.values(fakeOthelloMemory.currentLayout.board.tokens));
    fakeOthello.currentLayout.board.turn = fakeOthelloMemory.currentLayout.board.turn;

    // Simulate a move
    fakeOthello.clickOnCell(args.cell.row, args.cell.cell);
    fakeOthello.setScores();
    fakeOthello.displayPossibilities();

    // Send the new board
    ipcRenderer.send('simulateResponse', fakeOthello);
});