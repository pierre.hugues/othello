"use strict";

const {remote} = require("electron");

function init() {
    document.getElementById("min-btn").addEventListener("click", () => {
        const window = remote.getCurrentWindow();
        window.minimize();
    });

    document.getElementById("max-btn").addEventListener("click", () => {
        const window = remote.getCurrentWindow();
        if (window.isMaximized()) {
            window.unmaximize();
        } else {
            window.maximize();
        }
    });

    document.getElementById("close-btn").addEventListener("click", () => {
        const window = remote.getCurrentWindow();
        window.close();
    });
}

document.onreadystatechange = () => {
    if (document.readyState === "complete") {
        init();
    }
};