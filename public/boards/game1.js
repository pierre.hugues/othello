var tokens = {
    1: {
        color: "white",
        i: 3,
        j: 3,
    },
    2: {
        color: "black",
        i: 3,
        j: 4,
    },
    3: {
        color: "black",
        i: 4,
        j: 3,
    },
    4: {
        color: "black",
        i: 4,
        j: 4,
    },
    5: {
        color: "white",
        i: 2,
        j: 2,
    },
    6: {
        color: "white",
        i: 2,
        j: 4,
    },
    7: {
        color: "black",
        i: 2,
        j: 3,
    },
    8: {
        color: "white",
        i: 2,
        j: 5,
    },
    9: {
        color: "white",
        i: 4,
        j: 2,
    },
    10: {
        color: "black",
        i: 4,
        j: 5,
    },
    11: {
        color: "white",
        i: 4,
        j: 6,
    },
    12: {
        color: "black",
        i: 5,
        j: 2,
    },
    13: {
        color: "white",
        i: 5,
        j: 3,
    },
    14: {
        color: "white",
        i: 5,
        j: 4,
    },
    15: {
        color: "black",
        i: 5,
        j: 5,
    },
    16: {
        color: "white",
        i: 6,
        j: 1,
    },
};

module.exports = tokens;