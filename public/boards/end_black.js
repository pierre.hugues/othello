var tokens = {
    1: {
        color: "black",
        i: 3,
        j: 3,
    },
    2: {
        color: "black",
        i: 3,
        j: 4,
    },
    3: {
        color: "black",
        i: 4,
        j: 3,
    },
    4: {
        color: "white",
        i: 4,
        j: 4,
    },
};

module.exports = tokens;