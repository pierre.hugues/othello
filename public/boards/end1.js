var tokens = {
    1: {
        color: "black",
        j: 0,
        i: 0,
    },
    2: {
        color: "black",
        j: 2,
        i: 0,
    },
    3: {
        color: "black",
        j: 3,
        i: 0,
    },
    4: {
        color: "black",
        j: 4,
        i: 0,
    },
    5: {
        color: "black",
        j: 5,
        i: 0,
    },
    6: {
        color: "black",
        j: 1,
        i: 1,
    },
    7: {
        color: "black",
        j: 2,
        i: 1,
    },
    8: {
        color: "white",
        j: 3,
        i: 1,
    },
    9: {
        color: "white",
        j: 0,
        i: 2,
    },
    10: {
        color: "white",
        j: 1,
        i: 2,
    },
    11: {
        color: "black",
        j: 3,
        i: 2,
    },
    12: {
        color: "black",
        j: 4,
        i: 2,
    },
    13: {
        color: "white",
        j: 1,
        i: 3,
    },
    14: {
        color: "black",
        j: 2,
        i: 3,
    },
    15: {
        color: "black",
        j: 3,
        i: 3,
    },
    16: {
        color: "black",
        j: 4,
        i: 3,
    },
    17: {
        color: "white",
        j: 0,
        i: 4,
    },
    18: {
        color: "black",
        j: 2,
        i: 4,
    },
    19: {
        color: "white",
        j: 3,
        i: 4,
    },
    20: {
        color: "black",
        j: 4,
        i: 4,
    },
    21: {
        color: "white",
        j: 0,
        i: 5,
    },
    22: {
        color: "black",
        j: 1,
        i: 5,
    },
    23: {
        color: "black",
        j: 2,
        i: 5,
    },
    24: {
        color: "black",
        j: 3,
        i: 5,
    },
    25: {
        color: "black",
        j: 4,
        i: 5,
    },
    26: {
        color: "white",
        j: 0,
        i: 6,
    },
    27: {
        color: "black",
        j: 4,
        i: 6,
    }
};

module.exports = tokens;